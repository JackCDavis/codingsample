package com.jcd.dndcompanionapp.background

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.jcd.dndcompanionapp.repository.DnDRepository
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber

class SyncDatabaseWM (appContext: Context, params: WorkerParameters): CoroutineWorker(appContext, params),
    KoinComponent {
    val dataSyncRepository : DnDRepository? by inject()

    companion object{
        const val WORK_NAME="com.example.apidemokotlin.background.SyncDatabaseWM"
    }
    override suspend fun doWork(): Result {
        /**
         * Sync the backend API data with local database even if user is not using the app or device restarts
         */
        try{
            dataSyncRepository!!.refreshSpells()
            dataSyncRepository!!.refreshClasses()
            Timber.d("WorkManager: sync in progress")
        }
        catch (e:Exception){
            Timber.e("WorkManager error: ${e.localizedMessage}")
            return Result.retry()
        }
        return Result.success()
    }
}