package com.jcd.dndcompanionapp.network

import com.jcd.dndcompanionapp.database.entities.classentities.AllClasses
import com.jcd.dndcompanionapp.database.entities.classentities.Classes
import com.jcd.dndcompanionapp.database.entities.spellentities.Spell
import com.jcd.dndcompanionapp.database.entities.spellentities.Spells
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path

interface Dnd_APIServices {

    @GET(API_CALLS.SPELL_API)
    fun getAllSpells(): Deferred<Spells>

    @GET(API_CALLS.CLASS_API)
    fun getAllClasses(): Deferred<AllClasses>

    @GET( API_CALLS.CLASS_API + "{classPath}")
    fun getClass(@Path("classPath")classPath:String?): Deferred<Classes>

    @GET( API_CALLS.SPELL_API + "{spellPath}")
    fun getSpell(@Path("spellPath")spellPath:String?): Deferred<Spell>
}