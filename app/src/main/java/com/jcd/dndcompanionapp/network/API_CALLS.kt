package com.jcd.dndcompanionapp.network

object API_CALLS {
    const val BASE_URL = "https://www.dnd5eapi.co"
    const val SPELL_API = "/api/spells/"
    const val CLASS_API = "/api/classes/"
}