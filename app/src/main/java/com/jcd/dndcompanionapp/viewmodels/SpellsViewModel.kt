package com.jcd.dndcompanionapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jcd.dndcompanionapp.database.entities.classentities.AllClasses
import com.jcd.dndcompanionapp.database.entities.classentities.Classes
import com.jcd.dndcompanionapp.database.entities.spellentities.Spells
import com.jcd.dndcompanionapp.repository.DnDRepository
import com.jcd.dndcompanionapp.util.LoadingState
import kotlinx.coroutines.*
import java.io.IOException

class SpellsViewModel (private val dnDRepository: DnDRepository): ViewModel()
{
    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
        get() = _loadingState

    val spellListResults: LiveData<List<Spells>> = dnDRepository.spellsResults
    private val viewModelJob: CompletableJob = SupervisorJob()
    private val viewModelScope: CoroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        refreshDataFromRepository()
    }

    private fun refreshDataFromRepository() {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                dnDRepository.refreshSpells()
                _loadingState.value = LoadingState.LOADED
            } catch (networkError: IOException) {
                _loadingState.value = LoadingState.error(networkError.message)
            }
        }
    }

    private val _navigateToSelectedProperty = MutableLiveData<Spells>()
    val navigateToSelectedProperty: LiveData<Spells>
        get() = _navigateToSelectedProperty

    fun displayPropertyDetails(spellProperty: Spells) {
        _navigateToSelectedProperty.value = spellProperty
    }

    fun displayDetailsComplete() {
        _navigateToSelectedProperty.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}