package com.jcd.dndcompanionapp.modules

import com.jcd.dndcompanionapp.network.Dnd_APIServices
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    fun provideUserAPI(retrofit: Retrofit): Dnd_APIServices {
        return retrofit.create(Dnd_APIServices::class.java)
    }
    single { provideUserAPI(get()) }
}