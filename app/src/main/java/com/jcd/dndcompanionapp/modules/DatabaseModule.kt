package com.jcd.dndcompanionapp.modules

import android.app.Application
import androidx.room.Room
import com.jcd.dndcompanionapp.database.db.DnDDB
import com.jcd.dndcompanionapp.database.db.DnDDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    fun provideDatabase(application: Application): DnDDB {
        return Room.databaseBuilder(application, DnDDB::class.java, "DnD.database")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    fun provideDao(database: DnDDB): DnDDao {
        return database.dndDao
    }

    single { provideDatabase(androidApplication()) }
    single { provideDao(get()) }
}