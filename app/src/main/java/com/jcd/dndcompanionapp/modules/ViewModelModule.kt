package com.jcd.dndcompanionapp.modules

import com.jcd.dndcompanionapp.viewmodels.ClassViewModel
import com.jcd.dndcompanionapp.viewmodels.SpellsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ClassViewModel(get()) }
   viewModel { SpellsViewModel(get()) }
}