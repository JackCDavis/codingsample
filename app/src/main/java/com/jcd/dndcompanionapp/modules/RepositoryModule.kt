package com.jcd.dndcompanionapp.modules

import com.jcd.dndcompanionapp.database.db.DnDDB
import com.jcd.dndcompanionapp.network.Dnd_APIServices
import com.jcd.dndcompanionapp.repository.DnDRepository
import org.koin.dsl.module

val repositoryModule = module {
    fun provideRepository(api: Dnd_APIServices, dao: DnDDB): DnDRepository  {
        return DnDRepository(api, dao)
    }
    single { provideRepository(get(), get()) }
}