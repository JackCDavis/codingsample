package com.jcd.dndcompanionapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.google.firebase.messaging.*
import java.lang.Exception

class NotificationHandler: FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        try {
            /**
             * 1 Show notification and dismiss the message
             * 2 When user clicks on the notification; launch the app and desired screen
             * 3 Configure notification channels
             */
            showNotification(remoteMessage.data["title"], remoteMessage.data["message"])
        }catch (e: Exception){

        }
    }

    private fun showNotification(title: String?, body: String?) {

//2 When user clicks on the notification; launch the app and desired screen
        val intent = Intent(this, MainActivity::class.java)
        /**
         * If set, and the activity being launched is already running in the current task,
         * then instead of launching a new instance of that activity, all of the other
         * activities on top of it will be closed and this Intent will be delivered to the (now on top) old activity as a new Intent.
         */
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        // Pending Intent: By giving a PendingIntent to another application, you are granting it the right to perform the operation you have specified as if the other application was yourself
        //Flag indicating that this PendingIntent can be used only once.
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val channelID = getString(R.string.MyAppChannelID)
        val channelName = getString(R.string.MyAppChannelName)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        /**
         * Define notification channel
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            setUpNotificationChannels(channelID, channelName, notificationManager)

        }
        /**
         * Define notifications
         */
        val soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder= NotificationCompat.Builder(this, channelID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun setUpNotificationChannels(channelID: String, channelName: String, notificationManager: NotificationManager) {
        val channel= NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_LOW)
        channel.enableLights(true)
        channel.lightColor= Color.GREEN
        channel.enableVibration(true)
        notificationManager.createNotificationChannel(channel)
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.i("Token", "Refreshed Token+ ${token}")
    }
}