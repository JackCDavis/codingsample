package com.jcd.dndcompanionapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.jcd.dndcompanionapp.R
import com.jcd.dndcompanionapp.adapter.ClassListAdapter
import com.jcd.dndcompanionapp.adapter.ClassListClick
import com.jcd.dndcompanionapp.database.entities.classentities.AllClasses
import com.jcd.dndcompanionapp.database.entities.general.Results
import com.jcd.dndcompanionapp.databinding.ClassLayoutBinding
import com.jcd.dndcompanionapp.databinding.ClassListLayoutBinding
import com.jcd.dndcompanionapp.util.LoadingState
import com.jcd.dndcompanionapp.viewmodels.ClassViewModel
import kotlinx.android.synthetic.main.class_layout.*
import org.koin.android.ext.android.bind
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel
    private val classListViewModel: ClassViewModel by viewModel<ClassViewModel>()
    private var classListViewAdapter: ClassListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }


    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding: ClassLayoutBinding =
            DataBindingUtil.inflate(inflater, R.layout.class_layout, container, false)
        binding.setLifecycleOwner(viewLifecycleOwner)
        binding.viewmodel = classListViewModel
        classListViewAdapter = ClassListAdapter(ClassListClick {
            classListViewModel.displayPropertyDetails(it)
        })
        binding.root.findViewById<RecyclerView>(R.id.class_recycler_view).apply {
            layoutManager = GridLayoutManager(context, 1)
            adapter = classListViewAdapter
        }


//        classListViewModel.navigateToSelectedProperty.observe(viewLifecycleOwner, Observer {
//            if (null != it) {
//            this.findNavController().navigate(
//
//            )
//            classListViewModel.displayDetailsComplete()
//        }
//    })
        return binding.root
}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, 2)
                }
            }
        }
    }


    private fun setupObserver() {
        classListViewModel.classListResults.observe(
            viewLifecycleOwner,
            Observer<List<AllClasses>> { classes ->
                classes.apply {
                    classListViewAdapter?.results = classes
                }
            })

        classListViewModel.loadingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                LoadingState.Status.FAILED -> {
                    progressBar.visibility = View.GONE
                    val snackbar =
                        Snackbar.make(requireView(), it.msg.toString(), Snackbar.LENGTH_SHORT)
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            requireActivity(),
                            R.color.red
                        )
                    )
                    snackbar.show()

                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }
                LoadingState.Status.RUNNING -> {
                    progressBar.visibility = View.VISIBLE
                    val snackbar = Snackbar.make(requireView(), "Loading", Snackbar.LENGTH_SHORT)
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            requireActivity(),
                            R.color.yellow
                        )
                    )
                    snackbar.show()

                }
                LoadingState.Status.SUCCESS -> {
                    val snackbar = Snackbar.make(requireView(), "success", Snackbar.LENGTH_SHORT)
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            requireActivity(),
                            R.color.green
                        )
                    )
                    snackbar.show()


                    progressBar.visibility = View.GONE

                }
            }
        })

    }
}