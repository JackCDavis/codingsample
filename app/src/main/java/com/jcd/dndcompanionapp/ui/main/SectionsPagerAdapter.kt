package com.jcd.dndcompanionapp.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.jcd.dndcompanionapp.R

private val TAB_TITLES = arrayOf(
        R.string.tab_classes,
        R.string.tab_spells,
    R.string.tab_character
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager)
    : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragment : Fragment? = null


        when(position)
        {
            0-> fragment = PlaceholderFragment.newInstance(1)
            1-> fragment = SpellListFragment.newInstance(2)
            2-> fragment = CharacterFragment.newInstance(3)
        }
        return fragment!!
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 3
    }
}