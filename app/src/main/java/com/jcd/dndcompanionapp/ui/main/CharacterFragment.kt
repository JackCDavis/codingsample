package com.jcd.dndcompanionapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.jcd.dndcompanionapp.R
import com.jcd.dndcompanionapp.adapter.SpellListAdapter
import com.jcd.dndcompanionapp.adapter.SpellListClick
import com.jcd.dndcompanionapp.database.entities.spellentities.Spells
import com.jcd.dndcompanionapp.databinding.SpellLayoutBinding
import com.jcd.dndcompanionapp.util.LoadingState
import com.jcd.dndcompanionapp.viewmodels.SpellsViewModel
import kotlinx.android.synthetic.main.class_layout.*
import org.koin.android.viewmodel.ext.android.viewModel

class CharacterFragment : Fragment() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: SpellLayoutBinding =
            DataBindingUtil.inflate(inflater, R.layout.character_layout, container, false)
        binding.setLifecycleOwner(viewLifecycleOwner)



//        classListViewModel.navigateToSelectedProperty.observe(viewLifecycleOwner, Observer {
//            if (null != it) {
//                this.findNavController().navigate(
//                    FirstFragmentDirections.actionFirstFragmentToSecondFragment(it)
//                )
//                classListViewModel.displayDetailsComplete()
//            }
//        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //setupObserver()
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): CharacterFragment {
            return CharacterFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, 3)
                }
            }
        }
    }


  //  private fun setupObserver() {
//        spellListViewModel.spellListResults.observe(
//            viewLifecycleOwner,
//            Observer<List<Spells>> { spells ->
//                spells.apply {
//                    spellListViewAdapter?.results = spells
//                }
//            })
//
//        spellListViewModel.loadingState.observe(viewLifecycleOwner, Observer {
//            when (it.status) {
//                LoadingState.Status.FAILED -> {
//                    progressBar.visibility = View.GONE
//                    val snackbar =
//                        Snackbar.make(requireView(), it.msg.toString(), Snackbar.LENGTH_SHORT)
//                    snackbar.view.setBackgroundColor(
//                        ContextCompat.getColor(
//                            requireActivity(),
//                            R.color.red
//                        )
//                    )
//                    snackbar.show()
//
//                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
//                }
//                LoadingState.Status.RUNNING -> {
//                    progressBar.visibility = View.VISIBLE
//                    val snackbar = Snackbar.make(requireView(), "Loading", Snackbar.LENGTH_SHORT)
//                    snackbar.view.setBackgroundColor(
//                        ContextCompat.getColor(
//                            requireActivity(),
//                            R.color.yellow
//                        )
//                    )
//                    snackbar.show()
//
//                }
//                LoadingState.Status.SUCCESS -> {
//                    val snackbar = Snackbar.make(requireView(), "success", Snackbar.LENGTH_SHORT)
//                    snackbar.view.setBackgroundColor(
//                        ContextCompat.getColor(
//                            requireActivity(),
//                            R.color.green
//                        )
//                    )
//                    snackbar.show()
//
//
//                    progressBar.visibility = View.GONE
//
//                }
//            }
//        })
//
//    }
}