package com.jcd.dndcompanionapp.database.entities.spellentities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jcd.dndcompanionapp.database.entities.general.Subclass
import kotlinx.android.parcel.Parcelize







@Parcelize
@Entity
data class Spell(
    @PrimaryKey
    var id : String,
    var index : String?,
    var name : String?,
    var desc : List<String>?,
    var higherLevel : List<String>?,
    var page : String?,
    var range : String?,
    var components: List<String>?,
    var material : String?,
    var ritual : Boolean?,
    var duration : String?,
    var concentration : Boolean?,
    var castingTime : String?,
    var level : Integer?,
    var school : School?,
    var classes : List<ClassHolder>?,
    var subclasses : List<Subclass>?,
    var url : String?

): Parcelable
{}