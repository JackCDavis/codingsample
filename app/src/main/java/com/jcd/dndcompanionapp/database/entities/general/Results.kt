package com.jcd.dndcompanionapp.database.entities.general

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Results(
    @PrimaryKey
    var index : String?,
    var name : String?,
    var url : String?,
    var imageURL : String?
): Parcelable
{}