package com.jcd.dndcompanionapp.database.entities.classentities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class ProficiencyChoice(
    @PrimaryKey(autoGenerate = true)
    var id : Int,
    var choose: Int?,
    var type : String?,
    var from: List<From>?

): Parcelable
{}