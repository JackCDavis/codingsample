package com.jcd.dndcompanionapp.database.entities.spellentities

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class ClassHolder(
var name: String?,
@PrimaryKey
var url : String
): Parcelable
{}