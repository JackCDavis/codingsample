package com.jcd.dndcompanionapp.database.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jcd.dndcompanionapp.database.entities.spellentities.*
import com.jcd.dndcompanionapp.database.entities.classentities.*
import com.jcd.dndcompanionapp.database.entities.general.Results
import com.jcd.dndcompanionapp.database.entities.general.Subclass


class Converters {

    @TypeConverter
    fun fromString(value: String): List<String> {
        val listType = object : TypeToken<List<String>>() {
        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: List<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromSchool(list: School?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toSchool(value: String): School {
        val listType = object : TypeToken<School>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromClassHolderList(list: List<ClassHolder>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toClassHolderList(value: String): List<ClassHolder> {
        val listType = object : TypeToken<List<ClassHolder>>() {

        }.type
        return Gson().fromJson(value, listType)
    }
    @TypeConverter
    fun fromSubclassList(list: List<Subclass>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toSubclassList(value: String): List<Subclass> {
        val listType = object : TypeToken<List<Subclass>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromSavingThrowsList(list: List<SavingThrows>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toSavingThrowsList(value: String): List<SavingThrows> {
        val listType = object : TypeToken<List<SavingThrows>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromStartingEquipment(list: StartingEquipment?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toStartingEquipment(value: String): StartingEquipment {
        val listType = object : TypeToken <StartingEquipment>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromProficiencyList(list: List<Proficiency>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toProficiencyList(value: String): List<Proficiency> {
        val listType = object : TypeToken<List<Proficiency>>() {

        }.type
        return Gson().fromJson(value, listType)
    }
    @TypeConverter
    fun fromClassLevels(list: ClassLevels?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toClassLevels(value: String): ClassLevels {
        val listType = object : TypeToken<ClassLevels>() {

        }.type
        return Gson().fromJson(value, listType)
    }
    @TypeConverter
    fun fromProficiencyChoiceList(list: List<ProficiencyChoice>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toProficiencyChoiceList(value: String): List<ProficiencyChoice> {
        val listType = object : TypeToken<ProficiencyChoice>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromResultsList(list: List<Results>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toResultsList(value: String): List<Results> {
        val listType = object : TypeToken<List<Results>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

}