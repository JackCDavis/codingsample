package com.jcd.dndcompanionapp.database.entities.classentities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jcd.dndcompanionapp.database.entities.general.Subclass
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Classes(
    @PrimaryKey(autoGenerate = true)
    val _ID : Int,
    var class_levels : String?,
    var hit_die : Integer?,
    var id  : String?,
    var index : String?,
    var name : String?,
    var proficiencies : List<Proficiency>?,
    var proficiency_choices : List<ProficiencyChoice>?,
    var saving_throws : List<SavingThrows>?,
    var starting_equipment : String?,
    var subclasses : List<Subclass>?,
    var url : String?

):Parcelable
{}