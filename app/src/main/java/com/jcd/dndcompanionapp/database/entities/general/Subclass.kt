package com.jcd.dndcompanionapp.database.entities.general

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Subclass(
    var name: String?,
    @PrimaryKey
    var url : String
): Parcelable
{}