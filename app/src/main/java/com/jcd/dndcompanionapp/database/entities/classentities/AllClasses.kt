package com.jcd.dndcompanionapp.database.entities.classentities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jcd.dndcompanionapp.database.entities.general.Results
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class AllClasses(
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    var count: Int?,
    var results : List<Results>?

): Parcelable
{}