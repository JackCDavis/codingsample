package com.jcd.dndcompanionapp.database.entities.classentities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class SavingThrows(
    @PrimaryKey
    var url : String,
    var _class: String?

): Parcelable
{}