package com.jcd.dndcompanionapp.database.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jcd.dndcompanionapp.database.entities.classentities.AllClasses
import com.jcd.dndcompanionapp.database.entities.classentities.Classes
import com.jcd.dndcompanionapp.database.entities.spellentities.Spell
import com.jcd.dndcompanionapp.database.entities.spellentities.Spells

@Dao
interface DnDDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllClasses(classes: AllClasses)
    @Query("select * from AllClasses")
    fun getLocalClasses(): LiveData<List<AllClasses>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertClass(classes: List<Classes>)
    @Query("select * from Classes")
    fun getLocalClass(): LiveData<List<Classes>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllSpells(spells: Spells)
    @Query("select * from Spells")
    fun getLocalSpells(): LiveData<List<Spells>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSpell(spell: List<Spell>)
    @Query("select * from Spell")
    fun getLocalSpell(): LiveData<List<Spell>>



}

@Database(
    entities = [Spells::class, Spell::class, AllClasses::class, Classes::class],
    version = 2,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class DnDDB : RoomDatabase() {
    abstract val dndDao: DnDDao
}