package com.jcd.dndcompanionapp.database.entities.spellentities

import android.os.Parcelable
import androidx.room.*
import com.jcd.dndcompanionapp.database.entities.general.Results
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Spells(
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    var count: Int?,
    var results : List<Results>?
): Parcelable
{}