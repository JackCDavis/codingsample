package com.jcd.dndcompanionapp.database.entities.classentities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Proficiency(
    var name: String?,
    @PrimaryKey
    var url : String
): Parcelable
{}