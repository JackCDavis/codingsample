package com.jcd.dndcompanionapp.repository

import androidx.lifecycle.LiveData
import com.jcd.dndcompanionapp.database.entities.classentities.Classes
import com.jcd.dndcompanionapp.database.db.DnDDB
import com.jcd.dndcompanionapp.database.entities.classentities.AllClasses
import com.jcd.dndcompanionapp.database.entities.general.Results
import com.jcd.dndcompanionapp.database.entities.spellentities.Spell
import com.jcd.dndcompanionapp.database.entities.spellentities.Spells
import com.jcd.dndcompanionapp.network.Dnd_APIServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class DnDRepository (private val dndAPIServices : Dnd_APIServices, private val database: DnDDB) {
    suspend fun refreshClasses() {
        withContext(Dispatchers.IO)
        {
            Timber.d("Refresh classes is called")

            val classesList = dndAPIServices.getAllClasses().await()
            database.dndDao.insertAllClasses(classesList)

            var classList = ArrayList<Classes>()

           for (result: Results in classesList.results!!)
           {
               var url = result.url!!.split("/")
               var _class = dndAPIServices.getClass(url.get(3)).await()
               classList.add(_class)
           }
            database.dndDao.insertClass(classList)

        }
    }

    suspend fun refreshSpells() {
        withContext(Dispatchers.IO)
        {
            Timber.d("Refresh spells is called")

            val spellsList = dndAPIServices.getAllSpells().await()
            database.dndDao.insertAllSpells(spellsList)
            var spellList = ArrayList<Spell>()

            for (result: Results in spellsList.results!!)
            {
                var url = result.url!!.split("/")
                var spell = dndAPIServices.getSpell(url.get(3)).await()
                spellList.add(spell)
            }
            database.dndDao.insertSpell(spellList)
        }
    }
    val spellResults: LiveData<List<Spell>> = database.dndDao.getLocalSpell()
    val classResult: LiveData<List<Classes>> = database.dndDao.getLocalClass()
    val spellsResults: LiveData<List<Spells>> = database.dndDao.getLocalSpells()
    val classesResult: LiveData<List<AllClasses>> = database.dndDao.getLocalClasses()
}