package com.jcd.dndcompanionapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.jcd.dndcompanionapp.R
import com.jcd.dndcompanionapp.database.entities.classentities.AllClasses
import com.jcd.dndcompanionapp.database.entities.classentities.Classes
import com.jcd.dndcompanionapp.database.entities.general.Results
import com.jcd.dndcompanionapp.databinding.ClassListLayoutBinding
import timber.log.Timber

class ClassListAdapter (val callBack:ClassListClick) : RecyclerView.Adapter<ClassListHolder>() {


    var results: List<AllClasses> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassListHolder {
        val withDataBinding: ClassListLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            ClassListHolder.LAYOUT,
            parent,
            false
        )
        return ClassListHolder(withDataBinding)
    }

    override fun getItemCount() = results.size


    override fun onBindViewHolder(holder: ClassListHolder, position: Int) {
        holder.viewDataBinding.also {
            Timber.d("number of classes: " + results.size.toString())
             it.result = results[0].results!!.get(position)
            it.resultsCallBack = callBack
        }
    }
}

class ClassListHolder(val viewDataBinding: ClassListLayoutBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.class_list_layout
    }
}
class ClassListClick(val block: (Classes)->Unit){
    fun onClick(classes: Classes)= block(classes)
}