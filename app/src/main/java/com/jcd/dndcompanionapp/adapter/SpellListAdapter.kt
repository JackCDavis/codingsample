package com.jcd.dndcompanionapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.jcd.dndcompanionapp.R
import com.jcd.dndcompanionapp.database.entities.classentities.AllClasses
import com.jcd.dndcompanionapp.database.entities.classentities.Classes
import com.jcd.dndcompanionapp.database.entities.spellentities.Spell
import com.jcd.dndcompanionapp.database.entities.spellentities.Spells
import com.jcd.dndcompanionapp.databinding.ClassListLayoutBinding
import com.jcd.dndcompanionapp.databinding.SpellListLayoutBinding
import timber.log.Timber

class SpellListAdapter (val callBack:SpellListClick) : RecyclerView.Adapter<SpellListHolder>() {


    var results: List<Spells> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpellListHolder {
        val withDataBinding: SpellListLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            SpellListHolder.LAYOUT,
            parent,
            false
        )
        return SpellListHolder(withDataBinding)
    }

    override fun getItemCount() = results.size


    override fun onBindViewHolder(holder: SpellListHolder, position: Int) {
        holder.viewDataBinding.also {
            Timber.d("number of classes: " + results.size.toString())
            it.result = results[0].results!!.get(position)
            it.resultsCallBack = callBack
        }
    }
}

class SpellListHolder(val viewDataBinding: SpellListLayoutBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.spell_list_layout
    }
}
class SpellListClick(val block: (Spells)->Unit){
    fun onClick(spells: Spells)= block(spells)
}