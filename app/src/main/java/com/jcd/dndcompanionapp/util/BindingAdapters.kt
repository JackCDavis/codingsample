package com.jcd.dndcompanionapp.util

import android.graphics.BitmapFactory
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.jcd.dndcompanionapp.R

@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String?) {
    if (url != null)
        Glide.with(imageView.context).load(url).into(imageView)
    else {

        Glide.with(imageView.context).load(R.drawable.no_image).into(imageView)
    }

}